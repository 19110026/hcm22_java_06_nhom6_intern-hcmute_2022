package com.example.intern_tiki.vn.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class product_more_info_string {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_more_info_string_generator")
    @SequenceGenerator(name = "product_more_info_string_generator", sequenceName = "product_more_info_string_seq", allocationSize = 1)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "detail")
    private String detail;
    @Column(name = "productId")
    private String productId;
    @Column(name = "productTypeId")
    private String productTypeId;

    public product_more_info_string() {
    }

    public product_more_info_string(Long id, String name, String detail, String productId, String productTypeId) {
	super();
	this.id = id;
	this.name = name;
	this.detail = detail;
	this.productId = productId;
	this.productTypeId = productTypeId;
    }

    public String getDetail() {
	return detail;
    }

    public Long getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String getProduct_id() {
	return productId;
    }

    public String getProductId() {
	return productId;
    }

    public String getProductTypeId() {
	return productTypeId;
    }

    public void setDetail(String detail) {
	this.detail = detail;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setProduct_id(String product_id) {
	productId = product_id;
    }

    public void setProductId(String productId) {
	this.productId = productId;
    }

    public void setProductTypeId(String productTypeId) {
	this.productTypeId = productTypeId;
    }

}
