package com.example.intern_tiki.vn.products;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface product_more_info_file_responsitory extends CrudRepository<product_more_info_file, Long> {
    Long countByProductId(String id);

    Long countByProductIdAndProductTypeId(String id, String productTypeId);

    List<product_more_info_file> findByProductId(String id);

    List<product_more_info_file> findByProductIdAndProductTypeId(String id, String productTypeId);
}
