package com.example.intern_tiki.vn.Sign_up_Sign_in;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface User_ModelResponsitory extends CrudRepository<User_Model, Integer> {
    long countByEmailAndProvider(String email, Provider provider);

    User_Model findByEmail(String mail);

    User_Model findByEmailAndProvider(String email, Provider valueOf);

    Optional<User_Model> findByIdAndPassword(Integer id, String password);

    User_Model findByName(String name);
}
