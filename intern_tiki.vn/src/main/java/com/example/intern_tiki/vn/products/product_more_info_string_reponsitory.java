package com.example.intern_tiki.vn.products;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface product_more_info_string_reponsitory extends CrudRepository<product_more_info_string, Long> {
    Long countByProductId(String id);

    Long countByProductIdAndProductTypeId(String id, String productTypeId);

    List<product_more_info_string> findByProductId(String id);

    product_more_info_string findByProductIdAndNameAndDetail(String product_id, String name, String detail);

    List<product_more_info_string> findByProductIdAndProductTypeId(String id, String productTypeId);

    @Query(value = "SELECT DISTINCT product_id FROM(SELECT product_id, COUNT(*) AS CNT FROM(SELECT * FROM product_more_info_string WHERE name IN :name) AS C WHERE detail IN :detail GROUP BY product_id HAVING CNT >= :numcat) AS T ", nativeQuery = true)
    ArrayList<Long> findWithNameAndDetail(@Param("name") String[] name, @Param("detail") String[] detail,
	    @Param("numcat") Integer numcat);
}
