package com.example.intern_tiki.vn.security;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class CommenceEntryPoint implements AuthenticationEntryPoint, Serializable {
	private static final long serialVersionUID = 565662170056829238L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {

		response.sendRedirect("http://localhost:3000/sign-in");
	}
}