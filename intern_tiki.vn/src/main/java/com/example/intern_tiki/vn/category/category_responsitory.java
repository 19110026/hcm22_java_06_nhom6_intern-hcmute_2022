package com.example.intern_tiki.vn.category;

import java.util.ArrayList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface category_responsitory extends CrudRepository<category, Long> {
    Long countAllByName(String name);

    @Query(value = "select COUNT( * ) FROM ( SELECT DISTINCT ProductID FROM ( SELECT * FROM (( SELECT *,   COUNT(*) OVER (PARTITION BY productid) as Cnt FROM category WHERE name IN :name)) AS T  HAVING Cnt = :numcat) as b) as countAll", nativeQuery = true)
    Long countCategoryLong(@Param("name") ArrayList<String> name, @Param("numcat") Integer numCatInteger);

    @Query(value = "select  *  FROM ( SELECT DISTINCT ProductID FROM ( SELECT * FROM (( SELECT *,   COUNT(*) OVER (PARTITION BY productid) as Cnt FROM category WHERE name IN :name)) AS T  HAVING Cnt = :numcat) as b) as countAll", countQuery = "select COUNT( * ) FROM ( SELECT DISTINCT ProductID FROM ( SELECT * FROM (( SELECT *,   COUNT(*) OVER (PARTITION BY productid) as Cnt FROM category WHERE name IN :name)) AS T  HAVING Cnt = :numcat) as b) as countAll", nativeQuery = true)
//@Query(value="SELECT name,parentId,ProductID FROM category")
    Page<String> findAll(@Param("name") ArrayList<String> name, @Param("numcat") Integer numCatInteger,
	    Pageable pageable);

    ArrayList<category> findAllById(Long id);

    ArrayList<category> findAllByName(String name);

    ArrayList<category> findAllByParentId(Long old_parentLong);

    // Page<category> findByNameIn(String name,Pageable pageable);
    @Query(value = "SELECT COUNT(*) FROM category WHERE name = :name AND productid = :productid", nativeQuery = true)
    Integer findByNameAndProductID(@Param("name") String name, @Param("productid") String ProductId);

    // @Query(value = "SELECT * FROM category WHERE id = :id", nativeQuery = true)
    category findFirstById(Long id);

    category findFirstByIdAndProductID(Long id, String productid);

    category findFirstByName(String name);

    category findFirstByParentIdAndProductID(Long parentId, String productID);
}
