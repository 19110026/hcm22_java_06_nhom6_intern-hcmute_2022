package com.example.intern_tiki.vn.Sign_up_Sign_in;

import java.util.Map;

public interface SignUpSignInService {
    public abstract Map<Object, Object> deActivateUser(Integer id);

    public abstract Map<Object, Object> deActivateUserUserSite(Integer id, String password);

    public abstract Map<Object, Object> deleteUser(Integer id);

    public abstract Map<Object, Object> getUserById(Integer id);

    public abstract User_Model registerUser(register_req req);

    public abstract Map<Object, Object> updateLogin(updateLoginReq updateLoginReq);

    public abstract Map<Object, Object> verify(String verify, String mail);
}
