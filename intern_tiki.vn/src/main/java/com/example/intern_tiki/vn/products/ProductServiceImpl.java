package com.example.intern_tiki.vn.products;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.intern_tiki.vn.Sign_up_Sign_in.Sing_in_Sign_out_Controller;

@SuppressWarnings("unused")

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);
    @Autowired
    private product_responsitory product_responsitory;
    @Autowired
    private product_more_info_string_reponsitory product_more_info_string_reponsitory;
    @Autowired
    private product_more_info_file_responsitory product_more_info_file_responsitory;
    @Autowired
    private product_rating_responsitory product_rating_responsitory;
    @Autowired
    private user_review_respository user_review_respository;
    @Autowired
    private product_type_respository product_type_respository;

    @Override
    public Map<String, String> deleteProduct(Long id) {
	Map<String, String> aMap = new HashMap<>();
	try {
	    product_responsitory.deleteById(id);
	    aMap.put("status", "done");
	} catch (Exception e) {
	    aMap.put("status", "false");
	}
	return aMap;
    }

    @Override
    public Map<String, String> deleteProductMoreInfoFile(Long id) {
	Map<String, String> aMap = new HashMap<>();
	try {
	    product_more_info_file_responsitory.deleteById(id);
	    aMap.put("status", "done");
	} catch (Exception e) {
	    aMap.put("status", "false");
	}
	return aMap;
    }

    @Override
    public Map<String, String> deleteProductMoreInfoString(Long id) {
	Map<String, String> aMap = new HashMap<>();
	try {
	    product_more_info_string_reponsitory.deleteById(id);
	    aMap.put("status", "done");
	} catch (Exception e) {
	    aMap.put("status", "false");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> deleteProductType(Long productTypeId) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    product_type_respository.deleteById(productTypeId);
	    aMap.put("done", "done");
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> deleteRatingOfProduct(Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    product_rating_responsitory.deleteById(id);
	    aMap.put("status", "done");
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> deleteReview(Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    List<user_review> replyreviReviews = user_review_respository.findAllByReplyTo(id);
	    for (user_review user_review : replyreviReviews) {
		user_review_respository.deleteById(user_review.getId());
	    }
	    user_review_respository.deleteById(id);
	    aMap.put("status", "done");
	} catch (Exception e) {
	    aMap.put("status", "error");

	    aMap.put("recomment", "chose different id");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getFileProductType(String productId, String productTypeId) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put("result",
		    product_more_info_file_responsitory.findByProductIdAndProductTypeId(productId, productTypeId));
	    aMap.put("product Type", product_type_respository.findById(Long.valueOf(productTypeId)));
	    aMap.put("num element",
		    product_more_info_file_responsitory.countByProductIdAndProductTypeId(productId, productTypeId));
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getMoreInfoFileProduct(String id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put(id, product_more_info_file_responsitory.findByProductId(id));
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getMoreInfoStringProduct(String id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put(id, product_more_info_string_reponsitory.findByProductId(id));
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getProduct(Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put(id, product_responsitory.findById(id));
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getProductByNameAndPrizeAndDetail(String name, String lowPrize, String upPrize,
	    Integer page, String[] detail_name, String[] detail) {
	Map<Object, Object> aMap = new HashMap<>();
	Pageable pageable = PageRequest.of(page, 10, Sort.by("name").ascending());
	Page<Products> resPage;
	if (detail_name != null && detail != null) {
	    ArrayList<Long> productId = product_more_info_string_reponsitory.findWithNameAndDetail(detail_name, detail,
		    detail.length);
	    logger.warn(String.valueOf(productId));
	    resPage = product_responsitory.findAllFromNamAndPrizeAndDetail(name, Long.valueOf(lowPrize),
		    Long.valueOf(upPrize), productId, pageable);

	} else {
	    resPage = product_responsitory.findAllFromNamAndPrize(name, Long.valueOf(lowPrize), Long.valueOf(upPrize),
		    pageable);
	}
	List<Products> resultList = resPage.getContent();
	aMap.put("result", resultList);
	aMap.put("numPage", resPage.getTotalPages());
	aMap.put("totle object", resPage.getTotalElements());
	return aMap;
    }

    @Override
    public Map<Object, Object> getProductTypeOfProduct(Long productId) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.put("Result", product_type_respository.findAllByProductId(String.valueOf(productId)));
	return aMap;
    }

    @Override
    public Map<Object, Object> getRating(Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put("return", product_rating_responsitory.findById(id));
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getRatingOfProduct(Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put("return", product_rating_responsitory.findByProductId(id));
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getStringProductType(String productId, String productTypeId) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    aMap.put("result",
		    product_more_info_string_reponsitory.findByProductIdAndProductTypeId(productId, productTypeId));
	    aMap.put("product Type", product_type_respository.findById(Long.valueOf(productTypeId)));
	    aMap.put("num element",
		    product_more_info_string_reponsitory.countByProductIdAndProductTypeId(productId, productTypeId));
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getUserReview(Long productidLong, Long replyto, int page) {
	Map<Object, Object> aMap = new HashMap<>();
	Pageable pageable = PageRequest.of(page, 10, Sort.by("date").descending());
	Page<user_review> resultPage = user_review_respository.findAllReviewWithReply(productidLong, replyto, pageable);
	List<user_review> resultList = new ArrayList<user_review>();
	resultList = resultPage.getContent();
	aMap.put("reviews", resultList);
	aMap.put("all review of product", user_review_respository.countAllReview(productidLong, replyto));
	for (user_review user_review : resultList) {
	    Long tempLong = user_review_respository.countAllReview(user_review.getProductId(), user_review.getId());
	    if (tempLong > 0)
		;
	    aMap.put("num of reply to " + user_review.getId(), tempLong);
	}
	return aMap;
    }

    @Override
    public Map<String, String> insertProduct(Products products) {
	Map<String, String> aMap = new HashMap<>();
	try {
	    Products temProducts = null;
	    temProducts = product_responsitory.findAllByName(products.getName());
	    if (temProducts != null && temProducts.getId() != products.getId()) {
		aMap.put("error", "already have product, choose correct id");
	    } else {
		product_responsitory.save(products);
		aMap.put("status", "done");
		aMap.put("id", product_responsitory.findAllByName(products.getName()).getId().toString());
	    }
	} catch (Exception e) {
	    aMap.put("status", "false");
	}

	return aMap;
    }

    @Override
    public Map<String, String> insertUpdateProductMoreInfoFile(product_more_info_file product_more_info_file) {
	Map<String, String> aMap = new HashMap<>();
	try {
	    product_more_info_file_responsitory.save(product_more_info_file);
	    aMap.put("status", "done");
	    aMap.put("id", product_more_info_file.getId().toString());
	} catch (Exception e) {
	    aMap.put("status", "false");
	}

	return aMap;
    }

    @Override
    public Map<String, String> insertUpdateProductMoreInfoString(product_more_info_string product_more_info_string) {
	Map<String, String> aMap = new HashMap<>();
	try {
	    product_more_info_string tempInfo_string = product_more_info_string_reponsitory
		    .findByProductIdAndNameAndDetail(product_more_info_string.getProduct_id(),
			    product_more_info_string.getName(), product_more_info_string.getDetail());
	    logger.warn("done here");
	    if (tempInfo_string == null) {
		product_more_info_string_reponsitory.save(product_more_info_string);
		aMap.put("status", "done");
		aMap.put("id", product_more_info_string.getId().toString());
	    } else {
		aMap.put("error", "already have");
		aMap.put("id", tempInfo_string.getId().toString());
	    }
	} catch (Exception e) {
	    aMap.put("status", "false");
	    logger.warn(e.toString());
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> insertUpdateProductType(product_type product_type) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    product_type temProduct_type = null;
	    temProduct_type = product_type_respository.findAllByProductIdAndName(product_type.getProductId(),
		    product_type.getName());
	    if (temProduct_type != null && temProduct_type.getId() != product_type.getId()) {
		aMap.put("error", "choose correct id");
		return aMap;
	    } else {
		product_type_respository.save(product_type);
		aMap.put("done", "done");
		aMap.put("id", product_type.getId().toString());
	    }
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> insertUpdateRating(product_rating product_rating) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    product_rating temProduct_rating = product_rating_responsitory
		    .findByProductId(product_rating.getProductId());
	    if (temProduct_rating == null) {
		product_rating_responsitory.save(product_rating);
		aMap.put("status", "done");
		aMap.put("id", product_rating.getId().toString());
	    } else {
		aMap.put("status", "error");
		aMap.put("reason", "product got rating, set correct id of rating");
	    }
	    return aMap;
	} catch (Exception e) {
	    aMap.put("status", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> insertUpdateReview(user_review user_review) {
	Map<Object, Object> aMap = new HashMap<>();

	Optional<user_review> tempReview = null;
	Optional<user_review> tempReview2 = null;
	try {
	    if (user_review.getId() != null) {
		tempReview = user_review_respository.findById(user_review.getId());
	    }
	    if (tempReview != null) {
		if (user_review.getLiked() != tempReview.get().getLiked()) {
		    if (user_review.getLiked()) {
			tempReview2 = user_review_respository.findById(user_review.getReplyTo());
			if (tempReview2 != null) {
			    tempReview2.get().setNumLike(tempReview2.get().getNumLike() + 1);
			    user_review_respository.save(tempReview2.get());
			}
		    }
		}
	    }
	    user_review_respository.save(user_review);
	    aMap.put("done", "done");
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> searchProductByName(String name) {
	Map<Object, Object> aMap = new HashMap<>();
	Pageable pageable = PageRequest.of(0, 10, Sort.by("name").ascending());
	Page<Products> resPage = product_responsitory.findAllByNameContainingIgnoreCase(name, pageable);
	List<Products> resultList = resPage.getContent();
	int temp = 0;
	for (Products products : resultList) {
	    aMap.put(temp, products.getName());
	    temp++;
	}
	return aMap;
    }

}
