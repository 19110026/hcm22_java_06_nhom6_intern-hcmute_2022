package com.example.intern_tiki.vn.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class user_review {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_review_generator")
    @SequenceGenerator(name = "user_review_generator", sequenceName = "user_review_seq", allocationSize = 1)
    private Long id;
    @Column(name = "rate")
    private Long rate;
    @Column(name = "userId")
    private Integer userId;
    @Column(name = "productId")
    private Long productId;
    @Column(name = "detail")
    private String detail;
    @Column(name = "date")
    private String date;
    @Column(name = "replyTo")
    private Long replyTo;
    @Column(name = "numLike")
    private Long numLike;
    @Column(name = "liked")
    private Boolean liked;

    public user_review() {
	replyTo = Long.valueOf(-1);
	liked = false;
	numLike = Long.valueOf(0);
    }

    public user_review(Long id, Long rate, Integer userId, Long productId, String detail, String date, Long replyTo,
	    Long numLike, Boolean liked) {
	super();
	this.id = id;
	this.rate = rate;
	this.userId = userId;
	this.productId = productId;
	this.detail = detail;
	this.date = date;
	this.replyTo = replyTo;
	this.numLike = numLike;
	this.liked = liked;
    }

    public String getDate() {
	return date;
    }

    public String getDetail() {
	return detail;
    }

    public Long getId() {
	return id;
    }

    public Boolean getLiked() {
	return liked;
    }

    public Long getNumLike() {
	return numLike;
    }

    public Long getProductId() {
	return productId;
    }

    public Long getRate() {
	return rate;
    }

    public Long getReplyTo() {
	return replyTo;
    }

    public Integer getUserId() {
	return userId;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public void setDetail(String detail) {
	this.detail = detail;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setLiked(Boolean liked) {
	this.liked = liked;
    }

    public void setNumLike(Long numLike) {
	this.numLike = numLike;
    }

    public void setProductId(Long productId) {
	this.productId = productId;
    }

    public void setRate(Long rate) {
	this.rate = rate;
    }

    public void setReplyTo(Long replyTo) {
	this.replyTo = replyTo;
    }

    public void setUserId(Integer userId) {
	this.userId = userId;
    }

}
