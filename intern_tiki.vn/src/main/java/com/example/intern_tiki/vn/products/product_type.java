package com.example.intern_tiki.vn.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class product_type {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_type_generator")
    @SequenceGenerator(name = "product_type_generator", sequenceName = "product_type_seq", allocationSize = 1)
    private Long id;
    @Column(name = "productId")
    private String productId;
    @Column(name = "name")
    private String name;
    @Column(name = "remain")
    private Long remain;

    public product_type() {
    }

    public product_type(Long id, String productId, String name, Long remain) {
	super();
	this.id = id;
	this.productId = productId;
	this.name = name;
	this.remain = remain;
    }

    public Long getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String getProductId() {
	return productId;
    }

    public Long getRemain() {
	return remain;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setProductId(String productId) {
	this.productId = productId;
    }

    public void setRemain(Long remain) {
	this.remain = remain;
    }

}
