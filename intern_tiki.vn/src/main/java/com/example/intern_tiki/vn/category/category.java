package com.example.intern_tiki.vn.category;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class category {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_generator")
    @SequenceGenerator(name = "category_generator", sequenceName = "category_seq", allocationSize = 1)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "parentId")
    private Long parentId;
    @Column(name = "productID")
    private String productID;

    public category() {
	parentId = Long.valueOf(-1);
    }

    public category(Long id, String name, String ProductID, Long parentId) {
	this.name = name;
	this.id = id;
	productID = ProductID;
	this.parentId = parentId;
    }

    public Long getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public Long getParentId() {
	return parentId;
    }

    public String getProductID() {
	return productID;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setParentId(Long parentId) {
	this.parentId = parentId;
    }

    public void setProductID(String productID) {
	this.productID = productID;
    }

}
