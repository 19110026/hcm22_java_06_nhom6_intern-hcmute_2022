package com.example.intern_tiki.vn.products;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

@SuppressWarnings("unused")
public interface user_review_respository extends CrudRepository<user_review, Long> {
    Long countAllByLikedAndProductIdAndId(Boolean Liked, Long productId, Long id);

    @Query(value = "SELECT COUNT(*) FROM user_review WHERE product_id = :productId AND reply_to = :replyTo AND detail IS NOT NULL", nativeQuery = true)
    Long countAllReview(@Param("productId") Long productId, @Param("replyTo") Long replyTo);

    List<user_review> findAllByReplyTo(Long id);

    @Query(value = "SELECT * FROM user_review WHERE product_id = :productId AND reply_to = :replyTo AND detail IS NOT NULL", countQuery = "SELECT COUNT(*) AS C FROM user_review WHERE product_id = :productId AND reply_to = :replyTo AND detail IS NOT NULL", nativeQuery = true)
    Page<user_review> findAllReviewWithReply(@Param("productId") Long productId, @Param("replyTo") Long replyTo,
	    Pageable pageable);

    @Override
    Optional<user_review> findById(Long id);
}
