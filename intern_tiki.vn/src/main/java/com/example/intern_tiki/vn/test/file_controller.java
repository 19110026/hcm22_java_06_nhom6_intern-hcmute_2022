package com.example.intern_tiki.vn.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.example.intern_tiki.vn.Sign_up_Sign_in.Sing_in_Sign_out_Controller;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.InputStream;


@SuppressWarnings("unused")
@Controller
public class file_controller {
	private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);
	
	@Autowired 
	private fileDBRepo fileDBRepo;
	@RequestMapping(value = { "/get_file"})
	public @ResponseBody Map<String, Object> getfile() {
		List <fileDB> filelistDbs=  fileDBRepo.findAll();
		Map<String, Object> files = new HashMap<String, Object>();
		files.put("files", filelistDbs);
		/*for(int i = 0; i<filelistDbs.size();i++) {
			logger.info("inside "+i,filelistDbs.get(i).getName());
		}*/
		return files;
	}
	@RequestMapping(value= {"/1_image"}, produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<byte[]> getImage() throws IOException{
		List <fileDB> filelistDbs=  fileDBRepo.findAll();
		try {
		byte[] bytes =filelistDbs.get(0).getData();
		return ResponseEntity
				.ok()
				.contentType(MediaType.IMAGE_PNG)
				.body(bytes);}
		catch (Exception e) {
			InputStream input = getClass().getClassLoader().getResourceAsStream("templates/not_foundpng.png");
			byte[] bytes = new byte[input.available()];
			input.read(bytes);
			input.close();
			return ResponseEntity
					.ok()
					.contentType(MediaType.IMAGE_PNG)
					.body(bytes);
		}
	}
	@RequestMapping(value= {"/1_video"}, produces = "video/mp4")
	public ResponseEntity<byte[]> getVideo() throws IOException{
		List <fileDB> filelistDbs=  fileDBRepo.findAll();
		try {
		byte[] bytes =filelistDbs.get(2).getData();
		//byte[] bytes2 = new byte[1000000];
		//System.arraycopy(bytes, 0, bytes2, 0, 1000000);
		return ResponseEntity
				.ok()
				.contentType(MediaType.parseMediaType("video/mp4"))
				.header("Accept-Ranges", "bytes")
				.header("Content-Length", String.valueOf(bytes.length))
				.header("Content-Range", "bytes "+"0"+"-"+"1000000"+"/"+String.valueOf(bytes.length))
			//	.body(bytes2);}
				.body(bytes);
		}
		catch (Exception e) {
			InputStream input = getClass().getClassLoader().getResourceAsStream("templates/video_not_found.png");
			byte[] bytes = new byte[input.available()];
			input.read(bytes);
			input.close();
			return ResponseEntity
					.ok()
					.contentType(MediaType.IMAGE_PNG)
					.body(bytes);
		}
	}
	@RequestMapping(value= {"/default_image"}, produces = MediaType.IMAGE_PNG_VALUE)
	public ResponseEntity<byte[]> getImageDefault() throws IOException{
		List <fileDB> filelistDbs=  fileDBRepo.findAll();
		try {
		byte[] bytes =filelistDbs.get(1).getData();
		return ResponseEntity
				.ok()
				.contentType(MediaType.IMAGE_PNG)
				.body(bytes);
		}
		catch (Exception e) {
			InputStream input = getClass().getClassLoader().getResourceAsStream("templates/not_foundpng.png");
			byte[] bytes = new byte[input.available()];
			input.read(bytes);
			input.close();
			return ResponseEntity
					.ok()
					.contentType(MediaType.IMAGE_PNG)
					.body(bytes);
		}
	}
	@PostMapping(value = {"/upload_file"})
	public @ResponseBody Map<String,String> upfile(MultipartFile file) throws IOException {
		String fileNameString = StringUtils.cleanPath(file.getOriginalFilename());
		fileDB imageDb = new fileDB(fileNameString,file.getContentType(),file.getBytes());
		try{fileDBRepo.save(imageDb);
		Map<String,String> map = new HashMap<String,String>();
		map.put("done", "done");
		return map;}
		catch (Exception e) {
			Map<String,String> map = new HashMap<String,String>();
			map.put("done", e.toString());
			map.put("done",fileNameString);
			map.put("done", file.getContentType());
			return map;
		}
	}
	@GetMapping(value = {"/set_default_image"})
	public @ResponseBody Map<String, String> done() {
	Map<String,String> map = new HashMap<String,String>();
	map.put("done", "done");
	try {
		InputStream input = getClass().getClassLoader().getResourceAsStream("templates/default_profile.png");
		byte[] arr = new byte[input.available()];
		input.read(arr);
		input.close();
		fileDB fileDB = new fileDB( "default profile","image/png",arr);
		fileDBRepo.save(fileDB);
	} catch (IOException e) {
		e.printStackTrace();
	}

	return map;
}
}
