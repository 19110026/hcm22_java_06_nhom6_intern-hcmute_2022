package com.example.intern_tiki.vn.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.example.intern_tiki.vn.Sign_up_Sign_in.CustomOAuth2User;
import com.example.intern_tiki.vn.Sign_up_Sign_in.CustomOAuth2UserService;
import com.example.intern_tiki.vn.Sign_up_Sign_in.Provider;
import com.example.intern_tiki.vn.Sign_up_Sign_in.Sing_in_Sign_out_Controller;
import com.example.intern_tiki.vn.Sign_up_Sign_in.User_Model;
import com.example.intern_tiki.vn.Sign_up_Sign_in.User_ModelResponsitory;
import com.example.intern_tiki.vn.Sign_up_Sign_in.loginfillter;
import com.google.gson.Gson;

@Configuration
@SuppressWarnings({ "deprecation", "unused" })
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);
    @Autowired
    private DataSource dataSource;
    @Autowired
    private CommenceEntryPoint unauthorizedHandler;

    @Autowired
    private User_ModelResponsitory user_ModelResponsitory;
    @Autowired
    private CustomOAuth2UserService oauthUserService;

    @Value("${spring.admin.please.token.secreate}")
    private String privateKey;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
	auth.jdbcAuthentication().passwordEncoder(passwordEncoder())
		/* add line for password encode */
		.dataSource(dataSource)
		.usersByUsernameQuery(
			"select email, password, enabled from user_model where email=? AND provider = \"LOCAL\"")
		.authoritiesByUsernameQuery("select email, role from user_model where email=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
	loginfillter loginfillter = new loginfillter();
	loginfillter.setPrivateKeyString(privateKey);
	loginfillter.setAuthenticationManager(authenticationManager());
	loginfillter.setApplicationContext(getApplicationContext());
	http.addFilterAt(loginfillter, UsernamePasswordAuthenticationFilter.class);
	/*
	 * loginfillter.setRequiresAuthenticationRequestMatcher( new
	 * OrRequestMatcher(new AntPathRequestMatcher("http://localhost:3000/sign-in",
	 * "POST"), new AntPathRequestMatcher("http://localhost:8081/process-login",
	 * "POST"), new AntPathRequestMatcher("http://localhost:3000/", "POST")));
	 */
	http.cors();
	http.httpBasic().and().csrf()
		//
		// .disable()
		.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
		//
		.authorizeRequests()
		// .antMatchers("/").permitAll()
		.antMatchers("/process-login", "http://localhost:8081/login/oauth2/code/google",
			"http://localhost:8081/oauth2/authorization/google", "/", "/get_user_review",
			// "/*").permitAll()
			"/get_category", "/get_category_parent", "/get_product", "/product_more_info_string_get",
			"/product_more_info_file_get", "/get_rating", "/get_rating_of_product",
			"/get_file_product_type", "/get_String_product_type", "/get_product_type_of_product",
			"/get_product_by_name_and_prize", "/search_product_by_name", "/register_server_add",
			"/done_security_return", "/get_user_by_id", "/login_custom", "/verify")
		.permitAll()
		.antMatchers("http://localhost:8081/insert_update_review", "/dumy_post_req", "/dumy_get_req",
			"http://localhost:8081/deactivate_user_user_site")
		.hasAnyRole("USER").antMatchers(
			// "/dumy_post_req",
			//
			"http://localhost:8081/insert_update_product", "http://localhost:8081/delete_product",
			"http://localhost:8081/insert_update_product_more_string",
			"http://localhost:8081/delete_product_more_string",
			"http://localhost:8081/insert_update_product_more_file",
			"http://localhost:8081/delete_product_more_file",
			"http://localhost:8081/delete_rating_of_product", "http://localhost:8081/add_save_rating",
			"http://localhost:8081/insert_update_product_type", "http://localhost:8081/delete_product_type",
			"http://localhost:8081/insert_update_category",
			"http://localhost:8081/change_many_parent_of_category", "http://localhost:8081/delete_category",
			"http://localhost:8081/delete_user", "http://localhost:8081/deactivate_user")
		.hasAnyRole("ADMIN")
		.antMatchers("http://localhost:8081/delete_review", "http://localhost:8081/update_login")
		.hasAnyAuthority("USER", "ADMIN").anyRequest().authenticated().and().formLogin()

		// .defaultSuccessUrl("http://localhost:3000/")

		// .defaultSuccessUrl("/done_login",true)
		// .loginPage("http://localhost:3000/sign-in").permitAll()
		.loginPage("http://localhost:8081/login_custom").loginProcessingUrl("/process-login").permitAll()
		// .failureUrl("http://localhost:3000/sign-in?error=true")
		.and().exceptionHandling().and().logout().invalidateHttpSession(true).deleteCookies("JSESSIONID")
		.permitAll().logoutSuccessHandler(new LogoutSuccessHandler() {

		    @Override
		    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
			    Authentication authentication) throws IOException, ServletException {

			// response.sendRedirect("http://localhost:8081/done_security_return?str=done
			// logout");

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			Map<Object, Object> aMap = new HashMap<>();
			aMap.put("done", "Successful Logout");
			String jsonString = new Gson().toJson(aMap);
			out.print(jsonString);
			out.flush();

		    }
		}).and().oauth2Login().loginPage("/login_custom").userInfoEndpoint().userService(oauthUserService);
	http.oauth2Login().loginPage("/login").userInfoEndpoint().userService(oauthUserService).and()
		.successHandler(new AuthenticationSuccessHandler() {
		    @Override
		    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			    Authentication authentication) throws IOException, ServletException {
			CustomOAuth2User oauthUser = (CustomOAuth2User) authentication.getPrincipal();
			String oauth2ClientName = oauthUser.getOauth2ClientName();
			User_Model user_Model = new User_Model();
			if (user_ModelResponsitory.countByEmailAndProvider(oauthUser.getEmail(),
				Provider.valueOf(oauth2ClientName.toUpperCase())) == 0) {
			    user_Model.setEmail(oauthUser.getEmail());
			    user_Model.setName(oauthUser.getName());
			    user_Model.setEnabled(1);
			    user_Model.setProvider(Provider.valueOf(oauth2ClientName.toUpperCase()));
			    user_ModelResponsitory.save(user_Model);
			}
			user_Model = user_ModelResponsitory.findByEmailAndProvider(oauthUser.getEmail(),
				Provider.valueOf(oauth2ClientName.toUpperCase()));
			response.sendRedirect("http://localhost:8081/done_security_return?str=done login&userId="
				+ user_Model.getId());
			// PrintWriter out = response.getWriter();
			// response.setContentType("application/json");
			/*
			 * Map<Object, Object> aMap = new HashMap<>(); aMap.put("done",
			 * "Successful Authentication"); String jsonString = new Gson().toJson(aMap);
			 */
			// response.setCharacterEncoding("UTF-8");
			// out.print(jsonString);
			// out.flush();
		    }
		}).and().oauth2ResourceServer().jwt();
    }

    /*
     * curl test curl -X POST -H "Origin: http://localhost:3000/hell/sign-in" -H
     * "Content-type: application/json" -d
     * "{\"username\":\"come\",\"password\":\"come\"}"
     * http://localhost:8081/process-login curl -X POST -H
     * "Origin: http://localhost:3000/sign-in" -H
     * "Content-type: application/x-www-form-urlencoded" -d
     * "username=come&password=come" --verbose http://localhost:8081/process-login
     * curl -X POST -H "Origin: http://localhost:3000" -H
     * "Content-type: application/json" -d
     * "{\"name\":\"abc\",\"email\":\"abc\",\"password\":\"abc\"}"
     * http://localhost:8081/register_server_add
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {

	CorsConfiguration configuration = new CorsConfiguration();
	configuration.addAllowedOriginPattern("http://localhost:3000/**");
	configuration.addAllowedOrigin("http://localhost:3000");
	configuration.addAllowedOriginPattern("http://localhost:8081/**");
	configuration.addAllowedHeader("*");
	configuration.addAllowedMethod("*");
	configuration.setAllowCredentials(true);
	UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	source.registerCorsConfiguration("/**", configuration);
	return source;
    }

    public PasswordEncoder passwordEncoder() {
	return NoOpPasswordEncoder.getInstance();
    }
}
