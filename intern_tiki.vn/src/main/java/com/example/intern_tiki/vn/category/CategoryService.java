package com.example.intern_tiki.vn.category;

import java.util.Map;

public interface CategoryService {
    public abstract Map<String, String> changeParent(Long parent, String name);

    public abstract Map<String, String> deleteCategory(Long id);

    public abstract Map<Object, Object> getPagingNumObject(String[] name);

    public abstract Map<Object, Object> getParent(String[] name);

    public abstract Map<Object, Object> getProductFromCategory(String[] name, int page);

    public abstract Map<Object, Object> insertUpdateCategory(category category);
}
