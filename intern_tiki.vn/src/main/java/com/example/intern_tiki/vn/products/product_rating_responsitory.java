package com.example.intern_tiki.vn.products;

import org.springframework.data.repository.CrudRepository;

public interface product_rating_responsitory extends CrudRepository<product_rating, Long> {

    product_rating findByProductId(Long productId);

}
