package com.example.intern_tiki.vn.products;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface product_responsitory extends CrudRepository<Products, Long> {
    Products findAllByName(String name);

    Page<Products> findAllByNameContainingIgnoreCase(String name, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE (:name IS NULL OR name LIKE %:name%) AND ((:lowPrize IS NULL OR :upPrize IS NULL OR prize BETWEEN :lowPrize AND :upPrize))", countQuery = "SELECT COUNT(*) FROM products WHERE (:name IS NULL OR name LIKE %:name%) AND ((:lowPrize IS NULL OR :upPrize IS NULL OR prize BETWEEN :lowPrize AND :upPrize))", nativeQuery = true)
    Page<Products> findAllFromNamAndPrize(@Param("name") String name, @Param("lowPrize") Long lowPrize,
	    @Param("upPrize") Long upPrize, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE (:name IS NULL OR name LIKE %:name%) AND ((:lowPrize IS NULL OR :upPrize IS NULL OR prize BETWEEN :lowPrize AND :upPrize)) AND id IN :productId", countQuery = "SELECT COUNT(*) FROM products WHERE (:name IS NULL OR name LIKE %:name%) AND ((:lowPrize IS NULL OR :upPrize IS NULL OR prize BETWEEN :lowPrize AND :upPrize)) AND id IN :productId", nativeQuery = true)
    Page<Products> findAllFromNamAndPrizeAndDetail(@Param("name") String name, @Param("lowPrize") Long lowPrize,
	    @Param("upPrize") Long upPrize, @Param("productId") ArrayList<Long> productId, Pageable pageable);

    List<Products> findByIdIn(ArrayList<Long> id);
}
