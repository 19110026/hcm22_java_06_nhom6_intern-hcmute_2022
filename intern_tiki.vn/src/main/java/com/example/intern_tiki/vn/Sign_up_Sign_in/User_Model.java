package com.example.intern_tiki.vn.Sign_up_Sign_in;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User_Model {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "provider")
    @Enumerated(EnumType.STRING)
    private Provider provider = Provider.LOCAL;

    /*
     * @Column(name = "phone") private String phone;
     * 
     * @Column(name = "avatar") private String avatar;
     */
    @Column(name = "role")
    private String role = "USER";
//	@Column(name = "enabled", columnDefinition = "integer default '1'")
    @Column(name = "enabled")
    private Integer enabled = 0;
    @Column(name = "verify")
    private String verify = "";

    public String getEmail() {
	return email;
    }

    public Integer getEnabled() {
	return enabled;
    }

    public Integer getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String getPassword() {
	return password;
    }

    public Provider getProvider() {
	return provider;
    }

    public String getRole() {
	return role;
    }

    public String getVerify() {
	return verify;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setEnabled(Integer enabled) {
	this.enabled = enabled;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setProvider(Provider provider) {
	this.provider = provider;
    }

    public void setRole(String role) {
	this.role = role;
    }

    public void setVerify(String verify) {
	this.verify = verify;
    }

}
