package com.example.intern_tiki.vn.category;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.intern_tiki.vn.Sign_up_Sign_in.Sing_in_Sign_out_Controller;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@SuppressWarnings("unused")
@Controller
public class category_controler {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);
    @Autowired
    private CategoryServiceImpl categoryServiceImpl;

    @PostMapping(value = { "/change_many_parent_of_category" })
    public @ResponseBody Map<String, String> change_many_parent_of_category(@RequestBody String jsonBody) {
	String name = null;
	Long parent = null;
	JsonObject convertedObject = new Gson().fromJson(jsonBody, JsonObject.class);
	name = convertedObject.get("name").getAsString();
	parent = convertedObject.get("parent").getAsLong();

	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(categoryServiceImpl.changeParent(parent, name));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_category" })
    public @ResponseBody Map<String, String> delete_category(@RequestBody String id1) {
	Long id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsLong();

	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(categoryServiceImpl.deleteCategory(id));
	return aMap;
    }

    @GetMapping(value = { "/get_category" })
    public @ResponseBody Map<Object, Object> get_category(@RequestParam("name") String[] name,
	    @RequestParam("page") int page) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(categoryServiceImpl.getParent(name));
	aMap.putAll(categoryServiceImpl.getPagingNumObject(name));
	aMap.putAll(categoryServiceImpl.getProductFromCategory(name, page));
	return aMap;
    }

    @GetMapping(value = { "/get_category_parent" })
    public @ResponseBody Map<Object, Object> get_category_parent(@RequestParam("name") String[] name) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(categoryServiceImpl.getParent(name));
	aMap.putAll(categoryServiceImpl.getPagingNumObject(name));
	return aMap;
    }

    @PostMapping(value = { "/insert_update_category" })
    public @ResponseBody Map<Object, Object> insert_update_category(@RequestBody category category) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(categoryServiceImpl.insertUpdateCategory(category));
	return aMap;
    }
}
