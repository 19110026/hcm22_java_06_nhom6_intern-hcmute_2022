package com.example.intern_tiki.vn.Sign_up_Sign_in;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Controller
//@RequestMapping(path="/Sign-in")
@SuppressWarnings("unused")
public class Sing_in_Sign_out_Controller {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);

    @Value("${spring.mail.username}")
    private String emailFromApplicationProperties;
    @Autowired
    private SignUpSignInServiceImpl signUpSignInServiceImpl;

    @PostMapping("/deactivate_user")
    public @ResponseBody Map<Object, Object> deactivate_user(@RequestBody String id1) {
	Integer id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsInt();
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(signUpSignInServiceImpl.deActivateUser(id));
	return aMap;
    }

    @PostMapping("/deactivate_user_user_site")
    public @ResponseBody Map<Object, Object> deactivate_user_user_site(@RequestBody String id1) {
	String password = null;
	Integer id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	password = convertedObject.get("password").getAsString();
	id = convertedObject.get("id").getAsInt();
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(signUpSignInServiceImpl.deActivateUserUserSite(id, password));
	return aMap;
    }

    @DeleteMapping("/delete_user")
    public @ResponseBody Map<Object, Object> delete_user(@RequestBody String id1) {

	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	Integer id = null;
	id = convertedObject.get("id").getAsInt();
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(signUpSignInServiceImpl.deleteUser(id));
	return aMap;
    }

    @RequestMapping("/done_login")
    public @ResponseBody Map<String, String> done_login() {
	Map<String, String> aMap = new HashMap<>();
	aMap.put("status", "done req");
	return aMap;
    }

    @GetMapping("/done_security_return")
    public @ResponseBody Map<String, String> done_security_return(@RequestParam String str,
	    @RequestParam String userId) {
	Map<String, String> aMap = new HashMap<>();
	aMap.put("status", str);
	aMap.put("user id", userId);
	return aMap;
    }

    @RequestMapping("/fail_login")
    public @ResponseBody Map<String, String> failure_login() {
	Map<String, String> aMap = new HashMap<>();
	aMap.put("status", "error");
	return aMap;
    }

    @GetMapping(value = { "/get_user_by_id" })
    public @ResponseBody Map<Object, Object> get_user_by_id(@RequestParam("id") Integer id) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(signUpSignInServiceImpl.getUserById(id));
	return aMap;
    }

    @GetMapping(value = { "/login_custom" })
    public String login() {
	return "/login_custom";
	// return "redirect:http://localhost:3000/sign-in";
    }

    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public void method(HttpServletResponse httpServletResponse) {
	httpServletResponse.setHeader("Location", "https://vnexpress.net");
	httpServletResponse.setStatus(302);
    }

    @GetMapping(value = { "/register" })
    public String register() {
	return "/register";
	// return "redirect:http://localhost:3000/sign-up";
    }

    @PostMapping(value = { "/register_server_add" })
    public @ResponseBody User_Model register_server_add(@RequestBody register_req req) {
	User_Model user_Model = new User_Model();
	user_Model = signUpSignInServiceImpl.registerUser(req);
	return user_Model;
    }

    @PostMapping("/update_login")
    public @ResponseBody Map<Object, Object> update_login(@RequestBody updateLoginReq updateLoginReq) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(signUpSignInServiceImpl.updateLogin(updateLoginReq));
	return aMap;
    }

    @GetMapping("/verify")
    public @ResponseBody Map<Object, Object> verify(@RequestParam("verify") String verify,
	    @RequestParam("mail") String mail) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(signUpSignInServiceImpl.verify(verify, mail));
	return aMap;
    }
}
