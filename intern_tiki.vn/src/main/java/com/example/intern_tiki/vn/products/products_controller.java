package com.example.intern_tiki.vn.products;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.intern_tiki.vn.Sign_up_Sign_in.Sing_in_Sign_out_Controller;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@SuppressWarnings("unused")
@Controller
public class products_controller {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);

    @Autowired
    private ProductServiceImpl productServiceImpl;

    @PostMapping(value = { "/add_save_rating" })
    public @ResponseBody Map<Object, Object> add_rating(@RequestBody product_rating product_rating) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.insertUpdateRating(product_rating));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_product" })
    public @ResponseBody Map<String, String> delete_product(@RequestBody String id1) {
	Long id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsLong();
	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.deleteProduct(id));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_product_more_file" })
    public @ResponseBody Map<String, String> delete_product_more_file(@RequestBody String id1) {
	Long id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsLong();
	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.deleteProductMoreInfoFile(id));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_product_more_string" })
    public @ResponseBody Map<String, String> delete_product_more_string(@RequestBody String id1) {
	Long id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsLong();
	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.deleteProductMoreInfoString(id));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_product_type" })
    public @ResponseBody Map<Object, Object> delete_product_type(@RequestParam("productTypeId") Long productTypeId) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.deleteProductType(productTypeId));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_rating_of_product" })
    public @ResponseBody Map<Object, Object> delete_rating_of_product(@RequestBody String id1) {
	Long id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsLong();
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.deleteRatingOfProduct(id));
	return aMap;
    }

    @DeleteMapping(value = { "/delete_review" })
    public @ResponseBody Map<Object, Object> delete_review(@RequestBody String id1) {
	Long id = null;
	JsonObject convertedObject = new Gson().fromJson(id1, JsonObject.class);
	id = convertedObject.get("id").getAsLong();
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.deleteReview(id));
	return aMap;
    }

    @GetMapping(value = { "/product_more_info_file_get" })
    public @ResponseBody Map<Object, Object> get_more_file(@RequestParam("id") String id) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getMoreInfoFileProduct(id));
	return aMap;
    }

    @GetMapping(value = { "/product_more_info_string_get" })
    public @ResponseBody Map<Object, Object> get_more_string(@RequestParam("id") String id) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getMoreInfoStringProduct(id));
	return aMap;
    }

    @GetMapping(value = { "/get_product" })
    public @ResponseBody Map<Object, Object> get_product(@RequestParam("id") Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getProduct(id));
	return aMap;
    }

    @GetMapping(value = { "/get_product_by_name_and_prize" })
    public @ResponseBody Map<Object, Object> get_product_by_name_and_prize(@RequestParam("name") String name,
	    @RequestParam("page") Integer page,
	    @RequestParam(value = "lowPrize", required = false, defaultValue = "0") String lowPrize,
	    @RequestParam(value = "upPrize", required = false, defaultValue = "1000") String upPrize,
	    @RequestParam(value = "detail_name", required = false) String[] detail_name,
	    @RequestParam(value = "detail", required = false) String[] detail) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getProductByNameAndPrizeAndDetail(name, lowPrize, upPrize, page, detail_name,
		detail));
	return aMap;
    }

    @GetMapping(value = { "/get_file_product_type" })
    public @ResponseBody Map<Object, Object> get_product_type(@RequestParam("productId") String productId,
	    @RequestParam("productTypeId") String productTypeId) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getFileProductType(productId, productTypeId));
	return aMap;
    }

    @GetMapping(value = { "/get_product_type_of_product" })
    public @ResponseBody Map<Object, Object> get_product_type_of_product(@RequestParam("productId") Long productId) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getProductTypeOfProduct(productId));
	return aMap;
    }

    @GetMapping(value = { "/get_rating" })
    public @ResponseBody Map<Object, Object> get_rating(@RequestParam("id") Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getRating(id));
	return aMap;
    }

    @GetMapping(value = { "/get_rating_of_product" })
    public @ResponseBody Map<Object, Object> get_rating_of_product(@RequestParam("id") Long id) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getRatingOfProduct(id));
	return aMap;
    }

    @GetMapping(value = { "/get_String_product_type" })
    public @ResponseBody Map<Object, Object> get_String_product_type(@RequestParam("productId") String productId,
	    @RequestParam("productTypeId") String productTypeId) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getStringProductType(productId, productTypeId));
	return aMap;
    }

    @GetMapping(value = { "/get_user_review" })
    public @ResponseBody Map<Object, Object> get_user_review(@RequestParam("productid") Long productidLong,
	    @RequestParam("replyto") Long replyto, @RequestParam("page") int page) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.getUserReview(productidLong, replyto, page));
	return aMap;
    }

    @PostMapping(value = { "/insert_update_product" })
    public @ResponseBody Map<String, String> insert_update_product(@RequestBody Products products) {
	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.insertProduct(products));
	return aMap;
    }

    @PostMapping(value = { "/insert_update_product_more_file" })
    public @ResponseBody Map<String, String> insert_update_product_more_file(
	    @RequestBody product_more_info_file product_more_info_file) {
	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.insertUpdateProductMoreInfoFile(product_more_info_file));
	return aMap;
    }

    @PostMapping(value = { "/insert_update_product_more_string" })
    public @ResponseBody Map<String, String> insert_update_product_more_string(
	    @RequestBody product_more_info_string product_more_info_string) {
	Map<String, String> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.insertUpdateProductMoreInfoString(product_more_info_string));
	return aMap;
    }

    @PostMapping(value = { "/insert_update_product_type" })
    public @ResponseBody Map<Object, Object> insert_update_product_type(@RequestBody product_type product_type) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.insertUpdateProductType(product_type));
	return aMap;
    }

    @PostMapping(value = { "/insert_update_review" })
    public @ResponseBody Map<Object, Object> insert_update_review(@RequestBody user_review user_review) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.insertUpdateReview(user_review));
	return aMap;
    }

    @GetMapping(value = { "/search_product_by_name" })
    public @ResponseBody Map<Object, Object> search_product_by_name(@RequestParam("name") String name) {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.putAll(productServiceImpl.searchProductByName(name));
	return aMap;
    }
}
