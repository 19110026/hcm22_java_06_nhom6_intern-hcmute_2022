package com.example.intern_tiki.vn.Sign_up_Sign_in;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class loginfillter extends AbstractAuthenticationProcessingFilter implements ApplicationContextAware {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);
    private static SecretKeySpec secretKey;
    private static byte[] key;

    public static void setKey(final String myKey) {
	MessageDigest sha = null;
	try {
	    key = myKey.getBytes("UTF-8");
	    sha = MessageDigest.getInstance("SHA-1");
	    key = sha.digest(key);
	    key = Arrays.copyOf(key, 16);
	    secretKey = new SecretKeySpec(key, "AES");
	} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
	    e.printStackTrace();
	}
    }

    protected String privateKeyString = "loginFillterPrivateKeyChainForJwtsLogin1";
    protected String username1 = "";
    protected String password1 = "";
    private User_ModelResponsitory user_ModelResponsitory;
    protected String jwString = "";

    Cipher cipher = null;

    public loginfillter() {
	super(new OrRequestMatcher(new AntPathRequestMatcher("http://localhost:3000/sign-in", "POST"),
		new AntPathRequestMatcher("/process-login", "POST"),
		new AntPathRequestMatcher("http://localhost:3000/", "POST")));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
	    throws AuthenticationException {

	String username = null, password = null, authorization = null;
	logger.warn("inside");
	try {
	    logger.warn(request.getQueryString());
	    if (request.getParameter("username") != null && request.getParameter("password") != null) {
		username = request.getParameter("username");
		username = (username != null) ? username.trim() : "";
		password = request.getParameter("password");
		password = (password != null) ? password : "";
	    } else if (request.getHeader("Authorization") != null && request.getHeader("JwtToken") == null) {
		authorization = request.getHeader("Authorization").substring("Basic".length()).trim();
		logger.warn(authorization);
		String[] userPasswd = new String(Base64.getDecoder().decode(authorization)).split(":");
		if (userPasswd.length == 2) {
		    username = userPasswd[0];
		    password = userPasswd[1];
		}
	    } else if (request.getHeader("JwtToken") != null) {
		authorization = request.getHeader("JwtToken");
		logger.warn(authorization);
		Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(privateKeyString.getBytes()))
			.build().parseClaimsJws(authorization);
		username = claims.getBody().get("username", String.class);
		password = claims.getBody().get("password", String.class);
		logger.warn("ksdfsd " + username + "/" + password);
		setKey(privateKeyString);
		try {
		    cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		try {
		    cipher.init(Cipher.DECRYPT_MODE, secretKey);
		} catch (InvalidKeyException e1) {
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		}
		try {
		    username = new String(cipher.doFinal(Base64.getDecoder().decode(username)));
		    password = new String((cipher.doFinal(Base64.getDecoder().decode(password))));
		    logger.warn("ksdfsd+sdf " + username + "/" + password);

		} catch (IllegalBlockSizeException | BadPaddingException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	    } else {
		@SuppressWarnings("unchecked")
		Map<String, String> requestMap = new ObjectMapper().readValue(request.getInputStream(), Map.class);
		username = requestMap.get("username");
		password = requestMap.get("password");
	    }
	} catch (IOException e) {
	    logger.warn(e.toString());
	    throw new AuthenticationServiceException(e.getMessage(), e);
	}
	username1 = username;
	password1 = password;
	setKey(privateKeyString);
	try {
	    cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
	} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	try {
	    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	} catch (InvalidKeyException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	try {
	    username1 = Base64.getEncoder().encodeToString(cipher.doFinal(username1.getBytes()));
	    password1 = Base64.getEncoder().encodeToString(cipher.doFinal(password1.getBytes()));
	} catch (IllegalBlockSizeException | BadPaddingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	logger.warn(username1 + "/" + password1);
	jwString = Jwts.builder().setIssuer("Login").setSubject("UsernamePassword").claim("username", username1)
		.claim("password", password1)
		.signWith(Keys.hmacShaKeyFor(privateKeyString.getBytes()), SignatureAlgorithm.HS256).compact();
	username1 = username;
	password1 = password;

	setAuthenticationSuccessHandler(new AuthenticationSuccessHandler() {

	    @Override
	    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
		    Authentication authentication) throws IOException, ServletException {

		// response.sendRedirect("http://localhost:8081/done_security_return?str=done
		// login");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		Map<Object, Object> aMap = new HashMap<>();
		aMap.put("done", "Successful Authentication");
		aMap.put("token", jwString);
		User_Model user_Model = user_ModelResponsitory.findByEmailAndProvider(username1, Provider.LOCAL);
		aMap.put("User Name", username1);
		if (user_Model != null)
		    aMap.put("user id", user_Model.getId());
		String jsonString = new Gson().toJson(aMap);
		out.print(jsonString);
		out.flush();

	    }
	});

	UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);

	return getAuthenticationManager().authenticate(authRequest);
    }

    public String getPrivateKeyString() {
	return privateKeyString;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	user_ModelResponsitory = applicationContext.getBean(User_ModelResponsitory.class);

    }

    public void setPrivateKeyString(String privateKeyString) {
	this.privateKeyString = privateKeyString;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
	    FilterChain filter, Authentication authResult) throws IOException, ServletException {
	super.successfulAuthentication(request, response, filter, authResult);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
	    AuthenticationException authException) throws IOException, ServletException {

	// response.sendRedirect("http://localhost:8081/login_custom?error");

	response.setContentType("application/json");
	response.setCharacterEncoding("UTF-8");
	PrintWriter out = response.getWriter();
	Map<Object, Object> aMap = new HashMap<>();
	aMap.put("error", "Unsuccessful Authentication");
	String jsonString = new Gson().toJson(aMap);
	out.print(jsonString);
	out.flush();

    }

}