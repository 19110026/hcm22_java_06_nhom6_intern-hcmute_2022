package com.example.intern_tiki.vn.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class product_rating {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_rating_generator")
    @SequenceGenerator(name = "product_rating_generator", sequenceName = "product_rating_seq", allocationSize = 1)
    private Long id;
    @Column(name = "rate")
    private Long rate = Long.valueOf(0);
    @Column(name = "count")
    private Long count = Long.valueOf(0);
    @Column(name = "productId")
    private Long productId;

    public product_rating() {
	rate = Long.valueOf(0);
	count = Long.valueOf(0);
    }

    public product_rating(Long id, Long rate, Long count, Long productId) {
	super();
	this.id = id;
	this.rate = rate;
	this.count = count;
	this.productId = productId;
    }

    public Long getCount() {
	return count;
    }

    public Long getId() {
	return id;
    }

    public Long getProductId() {
	return productId;
    }

    public Long getRate() {
	return rate;
    }

    public void setCount(Long count) {
	this.count = count;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setProductId(Long productId) {
	this.productId = productId;
    }

    public void setRate(Long rate) {
	this.rate = rate;
    }

}
