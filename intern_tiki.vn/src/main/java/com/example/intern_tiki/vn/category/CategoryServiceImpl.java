package com.example.intern_tiki.vn.category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.intern_tiki.vn.Sign_up_Sign_in.Sing_in_Sign_out_Controller;
import com.example.intern_tiki.vn.products.Products;
import com.example.intern_tiki.vn.products.product_responsitory;

@SuppressWarnings("unused")
@Service
public class CategoryServiceImpl implements CategoryService {
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);

    @Autowired
    private category_responsitory category_responsitory;
    @Autowired
    private product_responsitory product_responsitory;

    @Override
    public Map<String, String> changeParent(Long parent, String name) {
	Map<String, String> aMap = new HashMap<>();

	ArrayList<category> categories = category_responsitory.findAllByName(name);
	if (categories == null) {
	    aMap.put("status", "can't find catetory");
	    return aMap;
	}
	for (category category : categories) {
	    category.setParentId(parent);
	    category_responsitory.save(category);
	}
	aMap.put("status", "done");
	return aMap;
    }

    @Override
    public Map<String, String> deleteCategory(Long id) {
	Map<String, String> aMap = new HashMap<>();
	category category_tempCategory = null;
	category_tempCategory = category_responsitory.findFirstById(id);
	try {
	    if (category_tempCategory != null) {
		if (category_responsitory.countAllByName(category_tempCategory.getName()) > 1) {
		    String nameString = category_tempCategory.getName();
		    category_responsitory.deleteById(id);
		    category parent = category_responsitory.findFirstByName(nameString);
		    ArrayList<category> categories = category_responsitory.findAllByParentId(id);
		    for (category category : categories) {
			category.setParentId(parent.getId());
			category_responsitory.save(category);
		    }

		} else {
		    ArrayList<category> categories = category_responsitory.findAllByParentId(id);
		    if (categories != null) {
			category_tempCategory.setProductID(null);
			category_responsitory.save(category_tempCategory);
		    } else {
			category_responsitory.deleteById(id);

		    }
		}
		aMap.put("done", "done");
	    } else {
		aMap.put("status", "id don't exist");
	    }
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getPagingNumObject(String[] name) {
	ArrayList<String> tempArrayList = new ArrayList<>(Arrays.asList(name));
	Map<Object, Object> aMap = new HashMap<>();
	Long numPLong = category_responsitory.countCategoryLong(tempArrayList, tempArrayList.size());
	aMap.put("num of object", numPLong);
	aMap.put("num of page", numPLong / 10 + 1);
	return aMap;
    }

    @Override
    public Map<Object, Object> getParent(String[] name) {
	Map<Object, Object> aMap = new HashMap<>();
	for (String aString : name) {
	    // logger.warn(aString);
	    category category_name = category_responsitory.findFirstByName(aString);
	    if (category_name == null) {
		aMap.put("parent of " + aString, "can't find parent declared for " + aString);
		continue;
	    } else if (category_name.getParentId() == -1) {
		aMap.put("parent of " + aString, "no parent of " + aString);
		continue;
	    }
	    category category_tempCategory = category_responsitory.findFirstById(category_name.getParentId());
	    if (category_tempCategory == null) {
		aMap.put("parent of " + aString, "can't find parent declared for " + aString);
		continue;
	    } else if (category_tempCategory.getParentId() == -1) {
		aMap.put("parent of " + aString, category_tempCategory.getName());
		continue;
	    }
	    aMap.put("parent of " + aString, category_tempCategory.getName());
	    String tempString = "";
	    while (category_tempCategory.getParentId() != -1 && category_tempCategory != null) {
		tempString = category_tempCategory.getName();
		category_tempCategory = category_responsitory.findFirstById(category_tempCategory.getParentId());

		if (category_tempCategory == null) {
		    aMap.put("parent of " + tempString, "can't find parent declared for " + tempString);
		} else {
		    if (category_tempCategory.getParentId() == -1) {
		    }
		    aMap.put("parent of " + tempString, category_tempCategory.getName());
		}
		tempString = category_tempCategory.getName();
	    }
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> getProductFromCategory(String[] name, int page) {
	Pageable pagingPageable = PageRequest.of(page, 10);
	Map<Object, Object> aMap = new HashMap<>();
	Page<String> resultCategories = null;
	ArrayList<String> tempArrayList = new ArrayList<>(Arrays.asList(name));
	resultCategories = category_responsitory.findAll(tempArrayList, tempArrayList.size(), pagingPageable);
	List<String> categories = new ArrayList<String>();
	categories = resultCategories.getContent();
	ArrayList<Long> idlist = new ArrayList<Long>();
	for (String category : categories) {
	    idlist.add(Long.valueOf(category));
	    // logger.warn(category);
	}
	List<Products> products = product_responsitory.findByIdIn(idlist);
	aMap.put("result products", products);
	return aMap;
    }

    @Override
    public Map<Object, Object> insertUpdateCategory(category category) {
	Map<Object, Object> aMap = new HashMap<>();
	{// logger.warn(category.getProductID());
	    Integer category2 = category_responsitory.findByNameAndProductID(category.getName(),
		    category.getProductID());
	    if (category2 == 1 && category.getId() == null) {
		aMap.put("error", "Have set already choose corret id");
		return aMap;
	    }
	    category category3 = category_responsitory.findFirstByName(category.getName());
	    // logger.warn(category.getName() + " " + category3.getName());
	    if (category3 != null && category3.getParentId() != category.getParentId()) {
		aMap.put("error", "change parent of category " + category.getName() + " first");
		return aMap;
	    }
	    category_responsitory.save(category);
	    category category4 = category_responsitory.findFirstById(category.getParentId());
	    category category_tempCategory = null;
	    if (category4 != null) {
		category_tempCategory = category_responsitory.findFirstByParentIdAndProductID(category4.getParentId(),
			category.getProductID());
	    }
	    while (true) {
		// logger.warn(category.getParentId() + " " + category.getProductID());
		if (category4 == null) {
		    // aMap.put("error", "parent dont exist");
		    break;
		} else if (category_tempCategory == null) {
		    category_tempCategory = new category();
		    category_tempCategory.setProductID(category.getProductID());
		    category_tempCategory.setName(category4.getName());
		    category_tempCategory.setParentId(category4.getParentId());
		    category_responsitory.save(category_tempCategory);
		    // logger.warn(category4.getName());
		}
		category4 = category_responsitory.findFirstById(category4.getParentId());
		if (category4 == null) {
		    // aMap.put("error", "parent dont exist");
		    break;
		}
		category_tempCategory = category_responsitory.findFirstByParentIdAndProductID(category4.getParentId(),
			category.getProductID());

	    }
	    aMap.put("done", "done");
	}
	return aMap;
    }

}
