package com.example.intern_tiki.vn.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.search.mapper.orm.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/demo")
public class MainController {
    @Autowired
    private UserRespository userRepository;

    @PersistenceContext
    private EntityManager em;

    @PostMapping(path = "/add") // Map ONLY POST Requests
    public @ResponseBody String addNewUser(@RequestParam String name, @RequestParam String email) {
	User n = new User();
	n.setName(name);
	n.setEmail(email);
	userRepository.save(n);
	return "Saved";
    }

    @GetMapping(path = "/dumy_get_req") // Map ONLY POST Requests
    public @ResponseBody Map<Object, Object> dumy_get_req() {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.put("done", "dumy get");
	return aMap;
    }

    @PostMapping(path = "/dumy_post_req") // Map ONLY POST Requests
    public @ResponseBody Map<Object, Object> dumy_post_req() {
	Map<Object, Object> aMap = new HashMap<>();
	aMap.put("done", "dumy post");
	return aMap;
    }

    @GetMapping(path = "/all_name")
    @ResponseBody
    public List<Object[]> findAllName(String hello) {
	hello = "hi how are ya";
	return userRepository.findProjects();
    }

    @GetMapping(path = "/all_with_query")
    @ResponseBody
    public Iterable<User> findByNameEndsWith(String hello) {
	hello = "hi how are ya";
	return userRepository.findByNameEndsWith("t");
    }

    @GetMapping(path = "/all_with_Hibernate")
    @ResponseBody
    public List<User> findByNameWithHibernate() {
	String terms = "";
	int limit = 50;
	int offset = 1;
	return Search.session(em).search(User.class).where(f -> f.match().fields("name", "email").matching(terms))
		.fetchHits(offset, limit);
    }

    @GetMapping(path = "/all_with_MySQL")
    @ResponseBody
    public Iterable<User> findByNameWithMySQL(String hello) {
	hello = "hi how are ya";
	return userRepository.findByNameWithMySQL("t");
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<User> getAllUsers() {
	return userRepository.findAll();
    }
}
