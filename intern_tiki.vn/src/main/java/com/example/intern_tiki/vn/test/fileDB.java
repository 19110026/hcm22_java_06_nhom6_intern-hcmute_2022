package com.example.intern_tiki.vn.test;

import javax.persistence.*;


@Entity
@Table(name = "fileDB")
public class fileDB {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer idString;
    
	private String name;

    private String type;

    @Lob
    private byte[] data;

    public Integer getIdString() {
		return idString;
	}
	public void setIdString(Integer idString) {
		this.idString = idString;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public fileDB(String name, String type, byte[] data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }
    public fileDB() {

    }
}
