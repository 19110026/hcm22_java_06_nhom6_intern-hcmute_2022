package com.example.intern_tiki.vn.products;

import java.util.Map;

public interface ProductService {
    public abstract Map<String, String> deleteProduct(Long id);

    public abstract Map<String, String> deleteProductMoreInfoFile(Long id);

    public abstract Map<String, String> deleteProductMoreInfoString(Long id);

    public abstract Map<Object, Object> deleteProductType(Long productTypeId);

    public abstract Map<Object, Object> deleteRatingOfProduct(Long id);

    public abstract Map<Object, Object> deleteReview(Long id);

    public abstract Map<Object, Object> getFileProductType(String productId, String productTypeId);

    public abstract Map<Object, Object> getMoreInfoFileProduct(String id);

    public abstract Map<Object, Object> getMoreInfoStringProduct(String id);

    public abstract Map<Object, Object> getProduct(Long id);

    public abstract Map<Object, Object> getProductByNameAndPrizeAndDetail(String name, String lowPrize, String upPrize,
	    Integer page, String[] detail_name, String[] detail);

    public abstract Map<Object, Object> getProductTypeOfProduct(Long productId);

    public abstract Map<Object, Object> getRating(Long id);

    public abstract Map<Object, Object> getRatingOfProduct(Long id);

    public abstract Map<Object, Object> getStringProductType(String productId, String productTypeId);

    public abstract Map<Object, Object> getUserReview(Long productidLong, Long replyto, int page);

    public abstract Map<String, String> insertProduct(Products products);

    public abstract Map<String, String> insertUpdateProductMoreInfoFile(product_more_info_file product_more_info_file);

    public abstract Map<String, String> insertUpdateProductMoreInfoString(
	    product_more_info_string product_more_info_string);

    public abstract Map<Object, Object> insertUpdateProductType(product_type product_type);

    public abstract Map<Object, Object> insertUpdateRating(product_rating product_rating);

    public abstract Map<Object, Object> insertUpdateReview(user_review user_review);

    public abstract Map<Object, Object> searchProductByName(String name);
}
