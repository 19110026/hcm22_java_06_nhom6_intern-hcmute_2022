package com.example.intern_tiki.vn.products;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

public interface product_type_respository extends CrudRepository<product_type, Long> {
    ArrayList<product_type> findAllByProductId(String productId);

    ArrayList<product_type> findAllByProductIdAndName(Long productId, String name);

    product_type findAllByProductIdAndName(String productId, String name);
}
