package com.example.intern_tiki.vn.Sign_up_Sign_in;

public class updateLoginReq {

    private String oldPassword;
    private String name;
    private String email;
    private String password;
    private Provider provider;

    public String getEmail() {
	return email;
    }

    public String getName() {
	return name;
    }

    public String getOldPassword() {
	return oldPassword;
    }

    public String getPassword() {
	return password;
    }

    public Provider getProvider() {
	return provider;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setOldPassword(String oldPassword) {
	this.oldPassword = oldPassword;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setProvider(Provider provider) {
	this.provider = provider;
    }

}
