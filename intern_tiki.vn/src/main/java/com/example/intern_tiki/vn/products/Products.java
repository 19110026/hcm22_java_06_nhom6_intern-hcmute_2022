package com.example.intern_tiki.vn.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

@Entity
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_generator")
    @SequenceGenerator(name = "product_generator", sequenceName = "product_seq", allocationSize = 1)
    private Long id;
    @Column(name = "prize")
    private String prize;
    @Lob
    @Column(name = "image")
    private byte[] image;
    @Column(name = "image_name")
    private String image_name;
    @Column(name = "name")
    private String name;
    @Column(name = "sold")
    private Integer sold;
    @Column(name = "remain")
    private Integer remain;

    public Products() {
    }

    public Products(Long id, String prize, byte[] image, String image_name, String name, Integer sold, Integer remain) {
	super();
	this.id = id;
	this.prize = prize;
	this.image = image;
	this.image_name = image_name;
	this.name = name;
	this.sold = sold;
	this.remain = remain;
    }

    public Long getId() {
	return id;
    }

    public byte[] getImage() {
	return image;
    }

    public String getImage_name() {
	return image_name;
    }

    public String getName() {
	return name;
    }

    public String getPrize() {
	return prize;
    }

    public Integer getRemain() {
	return remain;
    }

    public Integer getSold() {
	return sold;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setImage(byte[] image) {
	this.image = image;
    }

    public void setImage_name(String image_name) {
	this.image_name = image_name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setPrize(String prize) {
	this.prize = prize;
    }

    public void setRemain(Integer remain) {
	this.remain = remain;
    }

    public void setSold(Integer sold) {
	this.sold = sold;
    }

}
