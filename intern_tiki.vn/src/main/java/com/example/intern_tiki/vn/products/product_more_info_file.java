package com.example.intern_tiki.vn.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

@Entity
public class product_more_info_file {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_more_info_file_generator")
    @SequenceGenerator(name = "product_more_info_file_generator", sequenceName = "product_more_info_file_seq", allocationSize = 1)
    private Long id;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "data")
    private byte[] data;
    @Column(name = "type")
    private String type;
    @Column(name = "productId")
    private String productId;
    @Column(name = "productTypeId")
    private String productTypeId;

    public product_more_info_file() {

    }

    public product_more_info_file(Long id, String name, byte[] data, String type, String productId,
	    String productTypeId) {
	super();
	this.id = id;
	this.name = name;
	this.data = data;
	this.type = type;
	this.productId = productId;
	this.productTypeId = productTypeId;
    }

    public byte[] getData() {
	return data;
    }

    public Long getId() {
	return id;
    }

    public String getName() {
	return name;
    }

    public String getProduct_id() {
	return productId;
    }

    public String getProductId() {
	return productId;
    }

    public String getProductTypeId() {
	return productTypeId;
    }

    public String getType() {
	return type;
    }

    public void setData(byte[] data) {
	this.data = data;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setProduct_id(String product_id) {
	productId = product_id;
    }

    public void setProductId(String productId) {
	this.productId = productId;
    }

    public void setProductTypeId(String productTypeId) {
	this.productTypeId = productTypeId;
    }

    public void setType(String type) {
	this.type = type;
    }

}
