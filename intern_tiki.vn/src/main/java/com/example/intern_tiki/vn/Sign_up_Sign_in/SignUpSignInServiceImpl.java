package com.example.intern_tiki.vn.Sign_up_Sign_in;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

@Service
public class SignUpSignInServiceImpl implements SignUpSignInService {

    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(Sing_in_Sign_out_Controller.class);
    @Autowired
    private User_ModelResponsitory user_ModelResponsitory;
    @Autowired
    private JavaMailSender emailSender;
    @Value("${spring.mail.username}")
    private String emailFromApplicationProperties;
    @Value("${mail.deactivate.subject}")
    private String deactivateSubject;
    @Value("${mail.updateAccount.subject}")
    private String updateAccount;
    @Value("${mail.verifyAccount.subject}")
    private String verifyAccount;

    @Override
    public Map<Object, Object> deActivateUser(Integer id) {
	Map<Object, Object> aMap = new HashMap<>();
	Optional<User_Model> user_Model = user_ModelResponsitory.findById(id);
	if (user_Model.isPresent()) {
	    user_Model.get().setEnabled(0);
	    user_ModelResponsitory.save(user_Model.get());
	    aMap.put("status", "done");
	    sendEmailDeactivate(user_Model);
	} else {
	    aMap.put("error", "no user have id given");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> deActivateUserUserSite(Integer id, String password) {
	Map<Object, Object> aMap = new HashMap<>();
	Optional<User_Model> user_Model = user_ModelResponsitory.findByIdAndPassword(id, password);
	if (user_Model.isPresent()) {
	    user_Model.get().setEnabled(0);
	    aMap.put("status", "done");
	    sendEmailDeactivate(user_Model);
	} else {
	    aMap.put("error", "no user have id given");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> deleteUser(Integer id) {
	Map<Object, Object> aMap = new HashMap<>();
	user_ModelResponsitory.deleteById(id);
	aMap.put("status", "done");
	return aMap;
    }

    @Override
    public Map<Object, Object> getUserById(Integer id) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    // logger.warn(category.getProductID());
	    aMap.put(id, user_ModelResponsitory.findById(id));
	} catch (Exception e) {
	    aMap.put("error", "error");
	}
	return aMap;
    }

    @Override
    public User_Model registerUser(register_req req) {
	User_Model user_Model = new User_Model();
	user_Model.setName(req.getName());
	user_Model.setEmail(req.getEmail());
	user_Model.setPassword(req.getPassword());
	byte[] array = new byte[256]; // length is bounded by 7
	new Random().nextBytes(array);
	String generatedString = new String(array, Charset.forName("UTF-8"));
	StringBuffer r = new StringBuffer();
	int length = 10;
	// Append first 20 alphanumeric characters
	// from the generated random String into the result
	for (int k = 0; k < generatedString.length(); k++) {

	    char ch = generatedString.charAt(k);

	    if (((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) && (length > 0)) {

		r.append(ch);
		length--;
	    }
	}
	generatedString = r.toString();
	user_Model.setVerify(generatedString);
	if (user_ModelResponsitory.countByEmailAndProvider(req.getEmail(), Provider.LOCAL) == 0) {
	    String msg;
	    try {
		msg = StreamUtils.copyToString(new ClassPathResource("mail/mail_template.html").getInputStream(),
			Charset.defaultCharset());
		msg = msg.replace("localhost:8081/",
			"http://localhost:8081/verify?verify=" + generatedString + "&mail=" + user_Model.getEmail());
		sendEmail(msg, req.getEmail(), verifyAccount);
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    user_ModelResponsitory.save(user_Model);
	}
	user_Model.setVerify("");
	return user_Model;
    }

    public void sendEmail(String msg, String sendTo, String subject) {
	MimeMessage message = emailSender.createMimeMessage();
	try {
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setFrom(emailFromApplicationProperties);
	    helper.setTo(sendTo);
	    helper.setSubject(subject);
	    message.setContent(msg, "text/html; charset=utf-8");
	    emailSender.send(message);
	} catch (MessagingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void sendEmailDeactivate(Optional<User_Model> user_Model) {
	byte[] array = new byte[256]; // length is bounded by 7
	new Random().nextBytes(array);
	String generatedString = new String(array, Charset.forName("UTF-8"));
	StringBuffer r = new StringBuffer();
	int length = 10;
	// Append first 20 alphanumeric characters
	// from the generated random String into the result
	for (int k = 0; k < generatedString.length(); k++) {

	    char ch = generatedString.charAt(k);

	    if (((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9')) && (length > 0)) {

		r.append(ch);
		length--;
	    }
	}
	generatedString = r.toString();
	user_Model.get().setVerify(generatedString);
	if (user_ModelResponsitory.countByEmailAndProvider(user_Model.get().getEmail(),
		user_Model.get().getProvider()) != 0) {
	    String msg;
	    try {
		msg = StreamUtils.copyToString(new ClassPathResource("mail/deactivate_email.html").getInputStream(),
			Charset.defaultCharset());
		msg = msg.replace("localhost:8081/", "http://localhost:8081/verify?verify=" + generatedString + "&mail="
			+ user_Model.get().getEmail());
		sendEmail(msg, user_Model.get().getEmail(), deactivateSubject);
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
	user_ModelResponsitory.save(user_Model.get());
    }

    @Override
    public Map<Object, Object> updateLogin(updateLoginReq updateLoginReq) {
	Map<Object, Object> aMap = new HashMap<>();
	try {
	    User_Model user_Model = user_ModelResponsitory.findByEmailAndProvider(updateLoginReq.getEmail(),
		    updateLoginReq.getProvider());
	    if (user_Model != null && user_Model.getPassword().equals(updateLoginReq.getOldPassword())) {
		user_Model.setPassword(updateLoginReq.getPassword());
		user_Model.setName(updateLoginReq.getName());
		user_ModelResponsitory.save(user_Model);
		user_Model.setVerify("");
		aMap.put("done", user_Model);
		String msg;
		msg = StreamUtils.copyToString(
			new ClassPathResource("mail/change_password_template.html").getInputStream(),
			Charset.defaultCharset());
		msg = msg.replace("localhost:8081/name", user_Model.getName());
		msg = msg.replace("localhost:8081/password", user_Model.getPassword());
		msg = msg.replace("localhost:8081/email", user_Model.getEmail());
		sendEmail(msg, user_Model.getEmail(), updateAccount);
	    }
	} catch (Exception e) {
	    aMap.put("error", "check email username/ password");
	}
	return aMap;
    }

    @Override
    public Map<Object, Object> verify(String verify, String mail) {
	Map<Object, Object> aMap = new HashMap<>();
	User_Model user_Model = user_ModelResponsitory.findByEmailAndProvider(mail, Provider.LOCAL);
	if (user_Model != null) {
	    if (user_Model.getVerify().equals(verify)) {
		aMap.put("status", "activated account");
		user_Model.setEnabled(1);
		user_Model.setVerify("");
		user_ModelResponsitory.save(user_Model);

	    } else if (user_Model.getEnabled() == 0) {
		aMap.put("status", "wrong verify code");
	    } else {
		aMap.put("status", "account actived already");
	    }
	} else {
	    aMap.put("status", "no user found");
	}
	return aMap;
    }

}
