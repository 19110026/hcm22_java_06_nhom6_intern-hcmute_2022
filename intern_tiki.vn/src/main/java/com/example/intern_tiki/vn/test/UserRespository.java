package com.example.intern_tiki.vn.test;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public  interface UserRespository extends CrudRepository<User, Integer> {
    @Query(value = "select * from user c where c.name like %?1",nativeQuery = true)
    Iterable<User> findByNameEndsWith(String chars);

	@Query(value = "select name from user", nativeQuery = true)
	public List<Object[]> findProjects();
	
	@Query(value = "select * from user c where match( c.name, c.email) against( ?1 in BOOLEAN MODE)",nativeQuery = true)
	Iterable<User> findByNameWithMySQL(String chars);

}
