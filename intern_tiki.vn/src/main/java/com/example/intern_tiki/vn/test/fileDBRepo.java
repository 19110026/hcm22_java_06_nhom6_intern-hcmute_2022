package com.example.intern_tiki.vn.test;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface fileDBRepo  extends CrudRepository<fileDB, String>{
	List<fileDB> findAll();
}
